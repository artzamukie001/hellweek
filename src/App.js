import React from "react";
import { BrowserRouter as Router, Route,Link,NavLink  } from "react-router-dom";
import SignUp from './Containers/SignUp'
import SignIn from './Containers/SignIn'
import "./App.css";

function App() {
  return (
    <Router>
      <div className="App">
        <div class="App__Aside"></div>
        <div class="App__Form">
          <div class="PageSwitcher">
          <NavLink exact to ="/" activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign In
            </NavLink>
            <NavLink exact to ="/signup" activeClassName="PageSwitcher__Item--Active"
              className="PageSwitcher__Item"
            >
              Sign Up
            </NavLink>
          </div>

          <div className="FormTitle">
            <NavLink  exact to="/" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">
              Sign In
            </NavLink >
            Or
            <NavLink to ="/signup" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">
              Sign Up
            </NavLink>
          </div>
          <Route exact path="/" component={SignIn}>
          </Route>
          <Route exact path="/signup" component={SignUp}>
          </Route>
        </div>
      </div>
    </Router>
  );
}

export default App;
