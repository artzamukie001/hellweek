import React, {Component} from 'react'

class SignIn extends Component{

    goToMain = () => {
        this.props.history.push("/main");
      };
    render(){
        return(
            <div className="FormCenter">
            <form>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="email">
                  Email
                </label>
                <input
                  type="email"
                  id="email"
                  className="FormField__Input"
                  placeholder="Enter your email"
                  name="email"
                />
              </div>

              <div className="FormField">
                <label className="FormField__Label" htmlFor="password">
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  className="FormField__Input"
                  placeholder="Enter your password"
                  name="password"
                />
              </div>

              <div className="FormField">
                <button className="FormField__Button mr-20">Sign In</button>
                <a class="FormField__Link">Create an account</a>
              </div>
            </form>
          </div>
        )
    }
}

export default SignIn;