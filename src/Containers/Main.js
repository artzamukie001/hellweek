import React, { Component } from "react";
import {
  Layout,
  Menu,
  Button,
  Divider,
  Icon,
  Badge,
  List,
  Upload,
  Card
} from "antd";

import bbaa from '../images.jpg'

const { Header, Content, Footer, Sider } = Layout;
const menus = ["home", "favorite", "cart", "profile"];

class Main extends Component {
  state = {
    collapsed: false
  };

  goToProfile = () => {
    this.props.history.push("/profile");
  };

  goToMain = () => {
    this.props.history.push("/main");
  };

  goToLogin = () => {
    this.props.history.push("/");
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    return (
      <div>
        <Layout style={{ width: "100vw", height: "100vh" }}>
          <Sider
            trigger={null}
            collapsible
            collapsed={this.state.collapsed}
          >
            <h1 style={{ textAlign: "center", fontSize: "150%",color:'white' }}>AWARE</h1>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="inline"
              defaultSelectedKeys={["1"]}
            >
              <Menu.Item key="1" onClick={this.goToMain}>
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="home" /> */}
                <Icon style={{fontSize: "18px"}} type="home" theme="twoTone" />
                <span style={{ fontSize: "18px"}}>
                  Home
                </span>
              </Menu.Item>
              <Menu.Item key="2" onClick={this.goToProfile}>
                <Icon style={{fontSize: "18px"}} type="smile" theme="twoTone" />
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="user" /> */}
                <span style={{ fontSize: "18px"}}>
                  Profile
                </span>
              </Menu.Item>
              <Menu.Item key="3" onClick={this.goToLogin}>
                <Icon style={{fontSize: "18px"}} type="rocket" theme="twoTone" />
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="upload" /> */}
                <span style={{ fontSize: "18px"}}>
                  Logout
                </span>
              </Menu.Item>
            </Menu>
          </Sider>

          <Layout style={{ backgroundImage: "linear-gradient(to top left,#91D4C2,#45BB89, #3D82AB)" }}>
            <Header style={{ backgroundImage: `url(${bbaa})`, padding: 0 }}>
              <Icon
                style={{ marginLeft: "2%" }}
                className="trigger"
                type={this.state.collapsed ? "menu" : "menu"}
                onClick={this.toggle}
              />
            </Header>
            <Content
              style={{
                margin: "24px 16px",
                padding: 24,
                background: "#fff",
                minHeight: 280
              }}
            >
              Content
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default Main;
