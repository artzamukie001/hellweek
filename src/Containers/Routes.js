import React from 'react'
import { Route, Switch } from 'react-router-dom'
import SignUp from './SignUp'
import SignIn from './SignIn'
import Main from './Main'


function Routes(){
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/" component={SignUp} />
                <Route exact path="/main" component={Main} />
            </Switch>
        </div>
    )

}

export default Routes
